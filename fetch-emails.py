import argparse
import csv
import os.path
from pipedrive import get_data
import data_sync

# TODO iterate over API keys (possible outside of this script?) 
parser = argparse.ArgumentParser(
                    prog='fetch-emails',
                    description='Migrates all data from Pipedrive to HubSpot')
parser.add_argument('-p', '--pipedrive-api-key', required=True)
args = parser.parse_args()

pipedrive_api_key = args.pipedrive_api_key


def _clear_file_contents(file):
    open(file, 'w').close()

def _map_emails_addresses(pipedrive_email_list):
    return list(map(lambda email: {"email": f"{email['linked_person_name']} <{email['email_address']}>"}, pipedrive_email_list))

ALL_FETCHED_EMAILS_FILE = "output/fetch-emails-all.csv"
NEW_FETCHED_EMAILS_FILE = "output/fetch-emails-new.csv"
EMAIL_TO_CONTACT_ASSOCIATION_TYPE_ID = 198
EMAIL_TO_COMPANY_ASSOCIATION_TYPE_ID = 186
EMAIL_TO_DEAL_ASSOCIATION_TYPE_ID = 210
EMAIL_TO_TICKET_ASSOCIATION_TYPE_ID = 224

contact_id_mapping = data_sync.contact_mapping()

print(f"Found {len(contact_id_mapping)} contacts to process...")

processed_mails = {}
fetched_threads = {}
fresh_start = True

if os.path.isfile(ALL_FETCHED_EMAILS_FILE):
    fresh_start = False
    with open(ALL_FETCHED_EMAILS_FILE, newline='') as csvfile:
        reader = csv.reader(csvfile)
        next(reader) # skip header row
        for row in reader:
            processed_mails[int(row[0])] = True

number_of_previously_fetched_emails = len(processed_mails)

print(f"Found {number_of_previously_fetched_emails} fetched emails from prior runs...")


OUTPUT_HEADER = ["Pipedrive Email ID","Timestamp","HS Owner ID","HS Direction","Subject","Body HTML","From","To","CC","BCC"]
_clear_file_contents(NEW_FETCHED_EMAILS_FILE)

with (
    open(ALL_FETCHED_EMAILS_FILE, "a") as all_fetched_emails_file,
    open(NEW_FETCHED_EMAILS_FILE, "a") as new_fetched_emails_file
    ):
    all_fetched_emails_writer = csv.writer(all_fetched_emails_file)
    new_fetched_emails_writer = csv.writer(new_fetched_emails_file)

    new_fetched_emails_writer.writerow(OUTPUT_HEADER)
    if fresh_start:
        all_fetched_emails_writer.writerow(OUTPUT_HEADER)

    for count, pipedrive_id in enumerate([56544]): # TODO enumerate(contact_id_mapping):
        hubspot_id = contact_id_mapping[pipedrive_id]
        mails = get_data(auth_token=pipedrive_api_key, endpoint=f"persons/{pipedrive_id}/mailMessages", params={"include_body": 1}, limit=1)
        for mail in mails:
            mail = mail["data"]
            thread_id = int(mail["mail_thread_id"])

            if not thread_id in fetched_threads:
                thread = next(get_data(auth_token=pipedrive_api_key, endpoint=f"mailbox/mailThreads/{thread_id}", params={"include_body": 1}))

                fetched_threads[thread_id] = {
                    "deal_id": int(thread["deal_id"]),
                    "lead_id": int(thread["lead_id"]),
                    "project_id": int(thread["project_id"])
                }

            mail_id = mail["data"]["id"]

            if mail_id in processed_mails:
                continue

            #fetched_mail = next(get_data(auth_token=pipedrive_api_key, endpoint=f"mailbox/mailMessages/{mail_id}", params={"include_body": 1}))

            hubspot_payload = {
                "properties": {
                    "hs_timestamp": mail["message_time"],
                    "hubspot_owner_id": 0, # TODO get user ids mail["user_id"]
                    "hs_email_direction": "FORWARDED_EMAIL",
                    "hs_email_status": "SENT",
                    "hs_email_subject": mail["subject"],
                    "hs_email_html": mail["body"],
                    "hs_email_headers": str({
                        "from": {
                            "email": _map_emails_addresses(mail["from"])[0]
                        },
                        "to": _map_emails_addresses(mail["to"]),
                        "cc": _map_emails_addresses(mail["cc"]),
                        "bcc": _map_emails_addresses(mail["bcc"])
                    }),
                    "hs_attachement_ids": "" # TODO handle attachements
                },
                "associations": [
                    {
                        "to": {
                            "id": contact_id_mapping[int(mail["from"][0]["linked_person_id"])]
                        },
                        "types": [
                            {
                                "associationCategory": "HUBSPOT_DEFINED",
                                "associationTypeId": EMAIL_TO_CONTACT_ASSOCIATION_TYPE_ID
                            }
                        ]
                    }
                ]
            }

            if "deal_id" in fetched_threads[thread_id]:
                hubspot_id["associations"].append({
                    "to": {
                        "id": fetched_threads[thread_id]["deal_id"]
                    },
                    "types": [
                        {
                            "associationCategory": "HUBSPOT_DEFINED",
                            "associationTypeId": EMAIL_TO_DEAL_ASSOCIATION_TYPE_ID
                        }
                    ]
                })
            if "lead_id" in fetched_threads[thread_id]:
                pass # TODO handle leads
            if "project_id" in fetched_threads[thread_id]:
                pass # TODO handle projects

            # log to new emails first, in case something goes awry, the mail
            # will be fetched again as long as it's not written to all
            new_fetched_emails_writer.writerow([mail_id, hubspot_payload])
            all_fetched_emails_writer.writerow([mail_id, hubspot_payload])

            processed_mails[mail_id] = True

        if count % 100 == 0:
            number_of_newly_fetched_emails = len(processed_mails) - number_of_previously_fetched_emails
            print(f"Processed {count + 1} contact (fetched {number_of_newly_fetched_emails} emails)...")

if len(contact_id_mapping) % 100 != 0:
    number_of_newly_fetched_emails = len(processed_mails) - number_of_previously_fetched_emails
    print(f"Processed {len(contact_id_mapping)} contacts (fetched {number_of_newly_fetched_emails} emails).")
