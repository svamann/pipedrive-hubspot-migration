import json

def load():
    with open("env.json") as env_file:
        return json.load(env_file)
