import csv

def _get_mapping(mapping_file_path, key_header="Pipedrive Record ID", key_type=int, value_header="HubSpot Record ID", value_type=int):
    id_mapping = {}
    with open(mapping_file_path, "r") as mapping_file:
        reader = csv.reader(mapping_file)
        pipedrive_id_col_id = -1
        hubspot_id_col_id = -1
        for row in reader:
            if pipedrive_id_col_id == -1:
                pipedrive_id_col_id = int(row.index(key_header))
                hubspot_id_col_id = int(row.index(value_header))
            else:
                id_mapping[key_type(row[pipedrive_id_col_id])] = value_type(row[hubspot_id_col_id])
    return id_mapping
