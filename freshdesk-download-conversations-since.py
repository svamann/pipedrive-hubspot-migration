import requests
import json
import os

# TODO set these 4 parameters before running the script
# Only those tickets are retrieved that were updated after the "sinceDateTime"
sinceDateTime = "2024-04-21T11:20:14Z"
# If this is True, only those conversations are retrieved that were added after the "sinceDateTime", including the description
# If it's False, then we retrieve all conversations of the tickets that were updated after the "sinceDateTime"
filter_results = False
# Please provide your API_KEY here
API_KEY = "<inert your API key>"
output_file = f"data/data-sync/freshdesk-conversations-since-{sinceDateTime}.json"


# On first run we retrieve the tickets without conversations, then fill the conversations one by one
if not os.path.exists(output_file):
    tickets_without_conversations = []
    page = 1
    response_status = 200
    all_results_retrieved = False
    while not all_results_retrieved and response_status == 200 and page < 40:
        print("Retrieving page",page)
        tickets_response = requests.get(url = 'https://cqse.freshdesk.com/api/v2/tickets?per_page=100&page=' + str(page) + '&order_by=created_at&order_type=asc&updated_since=' + sinceDateTime, auth=(API_KEY, "X"))
        response_status = tickets_response.status_code
        if response_status == 200:
            print("Number of results:",len(tickets_response.json()))
            all_results_retrieved = len(tickets_response.json()) == 0
            tickets_without_conversations += tickets_response.json()
            page += 1

    with open(output_file, "w") as outfile:
        json.dump(tickets_without_conversations, outfile)

# Iterating the tickets: if there is already a conversation that was retrieved in a previous script run, skip; if not: retrieve conversations, and add it to the ticket; Iterate until Freshdesk refuses the request or we are done
with open(output_file, "r") as f:
    new_tickets = json.load(f)

for ticket in new_tickets:
    id = ticket['id']
    if "conversations" in ticket:
        print("Skipping ticket with id " + str(id) + " as it already has conversations filled")
        continue
    ticket_details = requests.get(url = "https://cqse.freshdesk.com/api/v2/tickets/" + str(id) + "?include=conversations,requester,company", auth=(API_KEY, "X"))
    status_code = ticket_details.status_code
    if status_code != 200:
        print(ticket_details.text)
        print(ticket_details.headers)
        # Error, continue from here
        print("Ran into error with id " + str(id) + ". Please rerun the script a bit later, until it retrieves data for all tickets.")
        break
    all_details = ticket_details.json()
    ticket["description"] = all_details["description"] if not filter_results or all_details["created_at"] >= sinceDateTime else None
    ticket["description_text"] = all_details["description_text"] if not filter_results or all_details["created_at"] >= sinceDateTime else None
    ticket["conversations"] = all_details["conversations"] if not filter_results else list(filter(lambda conversation: conversation["created_at"] >= sinceDateTime, all_details["conversations"]))
    ticket["requester"] = all_details["requester"]
    ticket["company"] = all_details["company"]
    print("Ticket with id",id,"done")

with open(output_file, "w") as outfile:
    json.dump(new_tickets, outfile, indent=4, sort_keys=True)

with open("data/data-sync/freshdesk-conversations.json", "r") as f:
    all_tickets = json.load(f)

all_tickets_by_id = dict((ticket['id'], ticket) for ticket in all_tickets)

for ticket in new_tickets:
    all_tickets_by_id[ticket['id']] = ticket

with open("data/data-sync/freshdesk-conversations.json", "w") as outfile:
    json.dump(list(all_tickets_by_id.values()), outfile, indent=4, sort_keys=True)
