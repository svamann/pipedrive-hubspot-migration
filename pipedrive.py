
import sys
import json
import urllib
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from datetime import *
from decimal import *
from collections import OrderedDict
from ratelimit import limits, sleep_and_retry

# main function entry point
def flexio_handler(flex):

    flex.output.content_type = 'application/x-ndjson'
    for data in get_data(flex.vars):
        flex.output.write(data)

def get_data(auth_token, endpoint, params={}, limit=sys.maxsize):
    url = f'https://api.pipedrive.com/v1/{endpoint}'

    page_size = min(500, limit)
    page_cursor_id = None
    while True:

        url_query_params = dict(params)
        url_query_params['limit'] = page_size
        if page_cursor_id is not None:
            url_query_params['start'] = page_cursor_id
        url_query_params['api_token'] = auth_token
        url_query_str = urllib.parse.urlencode(url_query_params)
        page_url = url + '?' + url_query_str

        content = _get(page_url)
        data = content.get('data',[])

        if data is None or len(data) == 0: # sanity check in case there's an issue with cursor
            break

        if isinstance(data, list):
            for item in data:
                yield item
        else:
            yield data

        has_more = content.get('additional_data',{}).get('pagination',{}).get('more_items_in_collection', False)
        if has_more == False:
            break

        page_cursor_id = content.get('additional_data',{}).get('pagination',{}).get('next_start')
        if page_cursor_id is None:
            break
        
        if page_cursor_id >= limit:
            break

#@sleep_and_retry
#@limits(calls=80, period=120)
def _get(page_url):
    response = requests_retry_session().get(page_url)
    response.raise_for_status()
    return response.json()

def requests_retry_session(
    retries=3,
    backoff_factor=2,
    status_forcelist=(429, 500, 502, 503, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session
